-- SUMMARY --

The eah modules displays your earned cobblestones from the Einstein@Home 
distributed computing environment.

For a full description of the module, visit the project page:
    http://drupal.org/project/eah

To submit bug reports and feature suggestions, or to track changes:
    http://drupal.org/project/issues/admin_menu


-- REQUIREMENTS --

None.


-- INSTALLATION --

* Install as usual, see 
    https://www.drupal.org/documentation/install/modules-themes/modules-7
  for further information.


-- CONTACT --

Current maintainers:
* Miroslav Shaltev (mirsha)

This project has been sponsored by:
* L3S Research Center
  The L3S is a research center for basic and applied research in the field of 
  Web Science. The L3S researchers develop new, innovative methods and 
  technologies that enable intelligent and seamless access to information 
  on the Web, link individuals and communities in all aspects of the knowledge 
  society – including science and education, and connect the Internet to the 
  real world and its institutions. Visit http://www.l3s.de for more information.